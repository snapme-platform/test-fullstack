//core
import React, { Component } from 'react'
import { Route, Router } from 'react-router-dom'
import { Switch, Redirect } from 'react-router-dom'
import { reduxForm } from 'redux-form'
import { connect } from 'react-redux';

//component
import Login from './Pages/Login/Login.pages'
import Signup from './Pages/Signup/Signup.pages'
import Home from './Pages/Home/Home.pages'
import Booking from './Pages/Booking/Booking.pages'
//plugin
import lodash from 'lodash';
import { HelmetProvider } from "react-helmet-async";

const PrivateRoute = ({ component: Component, ...rest }) => {
  if (localStorage.getItem('meAuth')) {
    return (
      <Route {...rest} render={props => (
        <Component {...props} />
      )} />
    )
  } 
  else {
    return (
      <Route {...rest} render={props => (
        <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
      )} />
    )
  }
}


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authMe: 1,
      register: 1,
      allPass: false

    }
  }

  render() {
    return (
      <>
        <HelmetProvider>
          <Switch>
            <>
              <PrivateRoute exact path="/" component={Home} />
              <PrivateRoute exact path='/mybooking' component={Booking} />
              <Route exact path="/login" component={Login} /> 
              <Route exact path="/signup" component={Signup} /> 
            </>

          </Switch>
        </HelmetProvider>
      </>
    )
  }
}

function mapStateToProps(state) {
  return {}
}
export default connect(mapStateToProps)(
  reduxForm({
    form: "App",
  })(App)
);