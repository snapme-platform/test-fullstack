import { publicServices } from "../services/index";
import swal from 'sweetalert'

function RegisterActions(params) {
    return dispatch => {
        publicServices.RegisterServices(params)
            .then((res) => {
                if (res.succces) {
                    localStorage.setItem('meAuth', res.data.data);
                    dispatch({
                        type: "REGISTER_SUCCESS",
                        data: res.data.data
                    })

                    swal("Register Successfully", "Let's enjoy", "success")
                        .then((value) => {
                            window.location.assign(`http://localhost:3000/`)
                        })
                } else {
                    swal("Register Failed", 'Error Message : ' + (res.data), "error");
                }
            });
    };
}

function LoginActions(params) {
    return dispatch => {
        publicServices.Login(params)
            .then((res) => {
                if (res.succces) {
                    localStorage.setItem('meAuth', res.data.data);
                    dispatch({
                        type: "LOGIN_SUCCESS",
                        data: res.data.data
                    })

                    swal("Login Successfully", "Let's enjoy", "success")
                        .then((value) => {
                            window.location.assign(`http://localhost:3000/`)
                        })
                } else {
                    swal("Register Failed", 'Error Message : ' + (res.data), "error");
                }
            });
    };
}

export const publicActions = {
    RegisterActions , LoginActions
}