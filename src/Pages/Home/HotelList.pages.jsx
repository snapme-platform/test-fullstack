import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import Card from "@material-ui/core/Card";
import { reduxForm } from "redux-form";
import { connect } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";

import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";


const MockData = [
  {
    src:
      "https://secure.ap-tescoassets.com/assets/TH/442/8850718801442/ShotType1_540x540.jpg",
    name: "เลย์ กลิ่นเอ็กซ์ตร้าบาร์บีคิว",
    price: "45",
    qty: "1",
  },
  {
    src:
      "https://secure.ap-tescoassets.com/assets/TH/893/8850718801893/ShotType1_540x540.jpg",
    name: "เลย์ รสโนริสาหร่าย",
    price: "45",
    qty: "1",
  },
  {
    src:
      "https://secure.ap-tescoassets.com/assets/TH/121/8850718801121/ShotType1_540x540.jpg",
    name: "เลย์ รสมันฝรั่งแท้",
    price: "45",
    qty: "1",
  },
];

class HotelList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ListProduct: MockData,
    };
  }
  openOrder(order) {
    const { dispatch } = this.props;
    dispatch({
      type: "OPEN_ORDER_DETAIL",
      data: order,
    });
  }
  renderProduct = () => {
    const { ListProduct } = this.state;
    let product = [];
    ListProduct.forEach((val, index) => {
      product.push(
        <div key={index} className="divRenderHotel">
          <Card>
            <CardActionArea>
              <CardMedia title="Contemplative Reptile">
                <img
                  className="imgSize"
                  src="https://r-cf.bstatic.com/images/hotel/max1024x768/251/251128999.jpg"
                  alt="recipe thumbnail"
                />
              </CardMedia>
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Lizard
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Lizards are a widespread group of squamate reptiles, with over
                  6,000 species, ranging across all continents except Antarctica
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                Share
              </Button>
              <Button size="small" color="primary">
                Learn More
              </Button>
            </CardActions>
          </Card>
        </div>
      );
    });
    return product;
  };

  render() {
    return (
      <div className="divLayer">
        <div className="divLayer2">
          {this.renderProduct()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  reduxForm({
    form: "homeother",
  })(HotelList)
);
