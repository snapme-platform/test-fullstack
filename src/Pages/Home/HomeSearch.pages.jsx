import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';

import HostelList from "./HotelList.pages";
import Menu from "../../Components/Menu/Menu.component";
import renderTextField from "../../Components/InputForm/InputForm";


class HomeSearch extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <div className="leyer">
          <h3>เลือกรูปแบบตามความต้องการ</h3>
          <form>
            <Grid
              container
              direction="row"
              justify="space-evenly"
              alignItems="center"
              spacing={3}
            >
              <Grid item xs={12} md={12} lg={6}>
                <Field
                  id="start"
                  name="start"
                  type="Date"
                  helperText="Check In"
                  margin="normal"
                  fullWidth
                  component={renderTextField}
                />
              </Grid>

              <Grid item xs={12} md={12} lg={6}>
                <Field
                 
                  id="end"
                  helperText="Check Out"
                  name="end"
                  type="Date"
                  margin="normal"
                  fullWidth
                  component={renderTextField}
                />
              </Grid>
              <Grid item xs={12} md={12} lg={6}>
                <Field
                  id="keyword"
                  name="keyword"
                  type="text"
                  fullWidth
                  helperText="Search for Hostel"
                  margin="normal"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchIcon />
                      </InputAdornment>
                    ),
                  }}
                  className="fieldSearch"
                  component={renderTextField}
                /> 
              </Grid>
              <Grid item xs={12} md={12} lg={6}>
                <div className="divSubmitSearch">
                  <button className="buttonSubmitSearch" type="submit">
                    Submit
                  </button>
                </div>
              </Grid>
            </Grid>
          </form>
        </div>
      </>
    );
  }
}

function mapStateToProps(state) {
  const { token } = state;
  return {
    token,
  };
}
export default connect(mapStateToProps)(
  reduxForm({
    form: "HomeSearch",
  })(HomeSearch)
);
