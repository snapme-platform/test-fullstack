import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import Card from "@material-ui/core/Card";
import { reduxForm } from "redux-form";
import { connect } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";

import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";

import MaterialTable from "material-table";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";


class HotelList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "Check In Date",
          field: "checkinDate",
          headerStyle: { width: "100px" },
        },
        {
          title: "Check Out Date",
          field: "checkOutDate",
          headerStyle: { width: "100px" },
        },
        { title: "Name", field: "name" },
        {
          title: "Status",
          field: "status",
        },
        { title: "Actions", field: "action" },
      ],
      data: [
        {
          checkinDate: "10-07-20",
          checkOutDate: "12-07-20",
          name: "จิ้งหรีด",
          status: "Confirmmed",
          action: this.setAction(),
        },
        {
            checkinDate: "10-07-20",
            checkOutDate: "12-07-20",
            name: "เอาทั้งคืน",
            status: "Confirmmed",
            action: this.setAction(),
          },
          {
            checkinDate: "10-07-20",
            checkOutDate: "12-07-20",
            name: "เย็ดข้ามปี",
            status: "Confirmmed",
            action: this.setAction(),
          }
          
      ],
    };
  }
  setAction = () => {
    return (
      <>
          <button className="buttonCancel">
            Cancel
          </button>
      </>
    );
  };
  openOrder(order) {
    const { dispatch } = this.props;
    dispatch({
      type: "OPEN_ORDER_DETAIL",
      data: order,
    });
  }
  renderProduct = () => {
    let product = [];
    product.push(
      <MaterialTable
        title='Reservation List'
        columns={this.state.columns}
        data={this.state.data}
        options={{
          search: true,
          toolbar: true,
        }}
      />
    );
    return product;
  };

  render() {
    return (
      <div className="layer1">
        <div className="layer2">
          {this.renderProduct()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(
  reduxForm({
    form: "homeother",
  })(HotelList)
);
