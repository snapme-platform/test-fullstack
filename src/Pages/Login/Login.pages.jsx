import React from 'react'
import { Link, withRouter,Redirect , useHistory} from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux';
import { publicActions } from "../../actions";

const validate = values => {
  const errors = {}

  const requiredFields = [
    'email',
    'password'

  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address'
  }

  return errors
}

const renderTextField = ({
  label,
  input,
  defaultValue,
  meta: { touched, invalid, error },
  ...custom
}) => (
    <TextField
      label={label}
      error={touched && invalid}
      helperText={touched && error}
      {...input}
      {...custom}
    />
  )


class Login extends React.Component {
  constructor(props) {
    super(props);
    
    
  }

  handleSubmit = (values) => {
    const { dispatch } = this.props;
    dispatch(publicActions.LoginActions(values));
  };

  goSignup =()=>{
    this.props.history.push('/signup');
  }

  

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="wrapper">
        <div className="form-wrapper">
          <h2 className="textLogin"> Login </h2>
          <div>
            <h5 className="textLogin"> กรุณากรอกข้อมูลให้ถูกต้อง </h5>
          </div>
          <form onSubmit={handleSubmit(this.handleSubmit)}>
            <div className="email">
              <Field
                id="email"
                name="email"
                label="Email"
                type="text"
                component={renderTextField}
              />
            </div>

           

            <div className="password">
              <Field
                id="password"
                name="password"
                label="Password"
                type="password"
                component={renderTextField}
              />
            </div>
            <div className="divSubmit">
              <button className="buttonSubmit" type="submit">Login</button>
            </div>
          </form>
          <button onClick={()=>{ return this.goSignup()}} className="buttonBack" >Sign up</button>
         
        </div>
      </div>

    )
  }
}


function mapStateToProps(state) {
  const { token } = state
  return {
    token
  }
}
export default connect(mapStateToProps)(reduxForm({
  form: 'login',
  validate
})(withRouter(Login)))