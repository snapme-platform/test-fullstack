import axios from 'axios'


function RegisterServices(params) {
    let req_header = {
        'Content-Type': 'application/json',
        'Charset': 'utf-8',
    }
    let temp = axios.post(`http://localhost:3001/signup`, params, { headers: req_header })
        .then(res => {
            if (res.data.statusCode === 200) {
                return {
                    data: res.data,
                    succces: true
                }
            } else {
                return {
                    errMsg: res.data.message,
                    data: res.data.data,
                    statusCode: res.data.statusCode
                }
            }
        })
    return temp;
}

function Login(params) {
    let req_header = {
        'Content-Type': 'application/json',
        'Charset': 'utf-8',
    }
    let temp = axios.post(`http://localhost:3001/login`, params, { headers: req_header })
        .then(res => {
            if (res.data.statusCode === 200) {
                return {
                    data: res.data,
                    succces: true
                }
            } else {
                return {
                    errMsg: res.data.message,
                    data: res.data.data,
                    statusCode: res.data.statusCode
                }
            }
        })
    return temp;
}

export const publicServices = {
    RegisterServices,
    Login
}