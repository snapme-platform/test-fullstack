const intitialState = {
    token: '',
    lineAuth: false,
    me : []
}

export function publicReducers(state = intitialState, action) {
    switch (action.type) {
        case "REGISTER_SUCCESS":
            return {
                ...state,
                token: action.data
            }
        case "LOGIN_SUCCESS":
            return {
                ...state
            }
        default: return state
    }
}